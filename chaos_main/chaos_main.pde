ArrayList<PVector> vertices;
float nextX;
float nextY;
final int verticesNUm=6;
final float fraction=0.56;
int previousR=0;

void setup() {
  size(800, 800);
  background( 2 ); 
  vertices = createPolygon(verticesNUm);
  strokeWeight(5);
  stroke(190, 50, 128);
  for (PVector vertex : vertices) {
    point(vertex.x, vertex.y);
  }
  strokeWeight(1);
  stroke(190, 190, 128);
  int r=(int)random(0, (float)verticesNUm);
  nextX=vertices.get(r).x;
  nextY=vertices.get(r).y;
}

ArrayList<PVector> createPolygon(int n) {
  final int offsetX=200;
  final int offsetY=100;
  final int magnitude=350;
  ArrayList<PVector> vertices=new ArrayList<PVector>();
  PVector offset=new PVector(offsetX, offsetY);
  for (int i=0; i<n; ++i) {
    float angle= 2*PI*i/n;
    PVector v=PVector.fromAngle(angle);
    v.mult(magnitude);
    v.add(offset);
    vertices.add(v);
    offset=v;
  }
  return vertices;
}

void draw() { 
  for (int i=0; i<100; ++i) {
    final int r=(int)random(0, (float)verticesNUm);
    if(r==previousR || (previousR+1)%verticesNUm==r || (previousR)%verticesNUm==(r+1)%verticesNUm)
    //if(r==previousR)
    {
      nextX=lerp(nextX, vertices.get(r).x, fraction);
      nextY=lerp(nextY, vertices.get(r).y, fraction);
      point(nextX, nextY);
      previousR=r;
    }
  }
}
